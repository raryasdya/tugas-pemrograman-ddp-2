package assignments.assignment1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class ExtractNPMFormatTanggalTest {

    @Test
    public void testExtractFormatTanggalOne() {
        String expected = "Tahun masuk: 2017\nJurusan: Ilmu Komputer\nTanggal Lahir: 07-10-1998";
        assertEquals(true, ExtractNPM.validate(17010710199814L));
        assertEquals(expected, ExtractNPM.extract(17010710199814L));
    }

    @Test
    public void testExtractFormatTanggalTwo() {
        String expected = "Tahun masuk: 2018\nJurusan: Ilmu Komputer\nTanggal Lahir: 23-08-2002";
        assertEquals(true, ExtractNPM.validate(18012308200219L));
        assertEquals(expected, ExtractNPM.extract(18012308200219L));
    }

    @Test
    public void testExtractFormatTanggalThree() {
        String expected = "Tahun masuk: 2018\nJurusan: Ilmu Komputer\nTanggal Lahir: 05-07-2000";
        assertEquals(true, ExtractNPM.validate(18010507200019L));
        assertEquals(expected, ExtractNPM.extract(18010507200019L));
    }

}