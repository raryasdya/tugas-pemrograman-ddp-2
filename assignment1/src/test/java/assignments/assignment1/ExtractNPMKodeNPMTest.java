package assignments.assignment1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class ExtractNPMKodeNPMTest {

    @Test
    public void testExtractKodeNPMOne() {
        String expected = "Tahun masuk: 2034\nJurusan: Ilmu Komputer\nTanggal Lahir: 23-11-2014";
        assertEquals(true, ExtractNPM.validate(34012311201419L));
        assertEquals(expected, ExtractNPM.extract(34012311201419L));
    }

    @Test
    public void testExtractKodeNPMTwo() {
        String expected = "Tahun masuk: 2099\nJurusan: Teknik Elektro\nTanggal Lahir: 29-09-2079";
        assertEquals(true, ExtractNPM.validate(99122909207992L));
        assertEquals(expected, ExtractNPM.extract(99122909207992L));
    }

}