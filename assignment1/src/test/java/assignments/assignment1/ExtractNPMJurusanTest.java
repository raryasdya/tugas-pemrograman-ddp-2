package assignments.assignment1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class ExtractNPMJurusanTest {

    @Test
    public void testExtractJurusanOne() {
        String expected = "Tahun masuk: 2018\nJurusan: Ilmu Komputer\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(18012311199917L));
        assertEquals(expected, ExtractNPM.extract(18012311199917L));
    }

    @Test
    public void testExtractJurusanTwo() {
        String expected = "Tahun masuk: 2018\nJurusan: Sistem Informasi\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(18022311199917L));
        assertEquals(expected, ExtractNPM.extract(18022311199917L));
    }

    @Test
    public void testExtractJurusanThree() {
        String expected = "Tahun masuk: 2018\nJurusan: Teknologi Informasi\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(18032311199917L));
        assertEquals(expected, ExtractNPM.extract(18032311199917L));
    }

    @Test
    public void testExtractJurusanFour() {
        String expected = "Tahun masuk: 2018\nJurusan: Teknik Telekomunikasi\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(18112311199917L));
        assertEquals(expected, ExtractNPM.extract(18112311199917L));
    }

    @Test
    public void testExtractJurusanFive() {
        String expected = "Tahun masuk: 2018\nJurusan: Teknik Elektro\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(18122311199917L));
        assertEquals(expected, ExtractNPM.extract(18122311199917L));
    }

    @Test
    public void testValidateJurusanOne() {
        assertEquals(false, ExtractNPM.validate(18002311199917L));
    }

    @Test
    public void testValidateJurusanTwo() {
        assertEquals(false, ExtractNPM.validate(18102311199917L));
    }

    @Test
    public void testValidateJurusanThree() {
        assertEquals(false, ExtractNPM.validate(18202311199917L));
    }

}