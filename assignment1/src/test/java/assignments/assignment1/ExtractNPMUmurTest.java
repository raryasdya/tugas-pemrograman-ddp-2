package assignments.assignment1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class ExtractNPMUmurTest {

    @Test
    public void testExtractUmurOne() {
        String expected = "Tahun masuk: 2014\nJurusan: Ilmu Komputer\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(14012311199917L));
        assertEquals(expected, ExtractNPM.extract(14012311199917L));
    }
    
    @Test
    public void testExtractUmurTwo() {
        String expected = "Tahun masuk: 2034\nJurusan: Ilmu Komputer\nTanggal Lahir: 23-11-1999";
        assertEquals(true, ExtractNPM.validate(34012311199919L));
        assertEquals(expected, ExtractNPM.extract(34012311199919L));
    }

    @Test
    public void testValidateUmurOne() {
        assertEquals(false, ExtractNPM.validate(13012311199917L));
    }

    @Test
    public void testValidateUmurTwo() {
        assertEquals(false, ExtractNPM.validate(13012311201414L));
    }

}