package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    /** Extract year of entry */
    public static String getYear(String npm) {
        return "20" + npm.substring(0, 2);
    }

    /** Extract major from its code */
    public static String getMajor(String npm) {
        String majorCode = npm.substring(2, 4);

        switch (majorCode) {
            case "01":
                return "Ilmu Komputer";
            case "02":
                return "Sistem Informasi";
            case "03":
                return "Teknologi Informasi";
            case "11":
                return "Teknik Telekomunikasi";
            case "12":
                return "Teknik Elektro";
            default:
                return "Invalid";
        }
    }

    /** Extract birth date (DD-MM-YYYY) */
    public static String getBirthDate(String npm) {
        String birthDate = npm.substring(4, 12);
        return (
            birthDate.substring(0, 2) + "-" +
            birthDate.substring(2, 4) + "-" +
            birthDate.substring(4)
        );
    }

    /** Validate age when enter college */
    public static boolean validateAge(String npm) {
        String birthDate = getBirthDate(npm);
        int birthYear = Integer.parseInt(birthDate.substring(6));
        int yearEntry = Integer.parseInt(getYear(npm));

        return yearEntry - birthYear >= 15;
    }

    /** Validate that the major exists */
    public static boolean validateMajor(String npm) {
        return !getMajor(npm).equals("Invalid");
    }

    /** Validate last digit of NPM */
    public static boolean validateNPM(String npm) {
        if (npm.length() != 14) {
            return false;
        }

        int kodeNPM = 0;
        for (int i = 0; i < 6; i++) {
            kodeNPM +=
                Character.getNumericValue(npm.charAt(i)) *
                Character.getNumericValue(npm.charAt(12 - i));
        }
        kodeNPM += Character.getNumericValue(npm.charAt(6));

        while (kodeNPM >= 10) {
            int temp = 0;
            while (kodeNPM > 0) {
                temp += kodeNPM % 10;
                kodeNPM /= 10;
            }

            kodeNPM = temp;
        }

        String strKode = "" + kodeNPM;
        return strKode.equals(npm.substring(13));
    }

    /** Check all validations */
    public static boolean validate(long npm) {
        String stringNPM = "" + npm;
        return (
            validateAge(stringNPM) &&
            validateMajor(stringNPM) &&
            validateNPM(stringNPM)
        );
    }

    /** Extract all outputs */
    public static String extract(long npm) {
        String stringNPM = "" + npm;
        return (
            "Tahun masuk: " + getYear(stringNPM) +
            "\nJurusan: " + getMajor(stringNPM) +
            "\nTanggal Lahir: " + getBirthDate(stringNPM)
        );
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            if (validate(npm)) {
                System.out.println(extract(npm));
            } else {
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();
    }
}
